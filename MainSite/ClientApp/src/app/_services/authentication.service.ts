import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { User } from '../_models';
import { environment } from '@environments/environment';

@Injectable({ providedIn: 'root' })
export class AuthenticationService {
    private currentUserSubject: BehaviorSubject<User>;
  public currentUser: Observable<User>;
  public username: string;

    constructor(private http: HttpClient) {
        this.currentUserSubject = new BehaviorSubject<User>(JSON.parse(localStorage.getItem('currentUser')));
        this.currentUser = this.currentUserSubject.asObservable();
    }

    public get currentUserValue(): User {
        return this.currentUserSubject.value;
    }

    login(username, password) {
        return this.http.post<any>(`${environment.apiUrl}/users/authenticate`, { username, password })
            .pipe(map(user => {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                localStorage.setItem('currentUser', JSON.stringify(user));
                this.currentUserSubject.next(user);
                return user;
            }));
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('currentUser');
        this.currentUserSubject.next(null);
    }

    sendPasswordResetLink(username) {
      return this.http.post<any>(`${environment.apiUrl}/users/sendPasswordResetLink`, { username })
            .pipe(map(user => {
                return user;
            }));
    }

  isResetPasswordTokenMatch(hashToken) {
      return this.http.post<any>(`${environment.apiUrl}/users/isresetPasswordTokenMatch`, { hashToken })
        .pipe(map(user => {
          this.username = user.username;
          return user;
        }));
  }

  SaveResetPassword(username, password) {
    return this.http.post<any>(`${environment.apiUrl}/users/SaveResetPassword`, { username, password });
      //.pipe(map(user => {
      //  this.username = user.username;
      //  return user;
      //}
      //);
  }

}
