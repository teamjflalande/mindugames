import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { first } from 'rxjs/operators';
import { User } from '../_models';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

import { AuthenticationService, AlertService } from '../_services';

@Injectable({ providedIn: 'root' })
export class AuthResetPasswordGuard implements CanActivate {
  matchHashCode = false;
  //currentUser: User;

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) {}

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return new Promise(res => {
      this.authenticationService.isResetPasswordTokenMatch(route.queryParams['key01'])
        .subscribe((user) => {
          if (user) {
            //this.router.navigate(['/resetpassword']);
            res(true);
          }
          else {
            this.router.navigate(['/confirmemail']);
            res(false);
          }
        },
          error => {
            this.alertService.error(error, true);          
            this.router.navigate(['/confirmemail']); // maybe display expired date
            res(false);
          }
        );
    });
  }


}
