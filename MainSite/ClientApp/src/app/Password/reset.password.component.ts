import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { User } from '../_models';

import { MustMatch } from '../_helpers/must-match.validator';

import { AlertService, AuthenticationService } from '../_services';

@Component({ templateUrl: 'reset.password.component.html' })
export class ResetPasswordComponent implements OnInit {
  currentUser: User;
    passwordForm: FormGroup;
    loading = false;
    submitted = false;
    //queryParamUserIdent: string;
    //queryParamHashCode: string;

    constructor(
        private formBuilder: FormBuilder,
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        private alertService: AlertService
    ) {
        // redirect to home if already logged in
        //if (this.authenticationService.currentUserValue) {
        //    this.router.navigate(['/']);
        //}
    } 
    ngOnInit() {
      this.passwordForm = this.formBuilder.group({
        username: [{ value: this.authenticationService.username, disabled: true }],
          newPassword: ['', [Validators.required, Validators.minLength(6)]],
          confirmPassword: ['', Validators.required]
      }, { validator: MustMatch('newPassword', 'confirmPassword') });

        // get return url from route parameters or default to '/'
      //this.passwordForm.controls['username'].setValue(this.authenticationService.username);
      //this.currentUser = this.route.snapshot.queryParams['user'];
    }

    // convenience getter for easy access to form fields
    get f() { return this.passwordForm.controls; }

    onSubmit() {
        this.submitted = true;

        // reset alerts on submit
        this.alertService.clear();

        // stop here if form is invalid
        if (this.passwordForm.invalid) {
            return;
        }

      this.loading = true;

      // update user password
      this.authenticationService.SaveResetPassword(this.f.username.value, this.f.confirmPassword.value)
            .pipe(first())
            .subscribe(
              data => {
                this.alertService.success("The password has been changed.", true);
                this.router.navigate(['/login']);   
                },
                error => {
                    this.alertService.error(error);
                    this.loading = false;
                });
    }
}
